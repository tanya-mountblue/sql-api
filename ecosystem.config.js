
module.exports = {
  apps : [{
    name: 'API',
    script: 'index.js',

    // Options reference: https://pm2.io/doc/en/runtime/reference/ecosystem-file/
    args: 'one two',
    instances: 1,
    autorestart: true,
    watch: false,
    max_memory_restart: '1G',
    env: {
      NODE_ENV: 'development'
    },
    env_production: {
      NODE_ENV: 'production'
    }
  }],

  deploy : {
    production : {
      key : "/home/dev/tanya/App_microservices/sql/express_app.pem",
      user : 'ubuntu',
      host : '18.222.53.137',
      ref  : 'origin/master',
      repo : 'git@gitlab.com:/mountblue/dec-2018-js-backend/07_tanya_mysql-api.git',
      path : '/home/ubuntu/07_tanya_mysql-api/',
      'post-deploy' : 'npm install && pm2 reload ecosystem.config.js --env production'
    }
  }
};
