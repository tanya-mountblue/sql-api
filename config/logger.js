const winston = require('winston');


const logger = winston.createLogger({
  levels: winston.config.syslog.levels,
  exitOnError: false,

  transports: [new winston.transports.File({
    level: 'info',
    filename: './config/logger.log',
  }),
  new winston.transports.Console()],

});


module.exports = logger;
