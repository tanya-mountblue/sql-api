// eslint-disable-next-line radix
// eslint-disable-next-line no-unused-vars
const mysql = require('promise-mysql');
const logger = require('../../config/logger');
const connection = require('../../database/mysql_connection');

async function Getbooks() {
  try {
    const conn = await connection;
    const books = await conn.query('select authors.Name as a,books.id,books.isbn,books.Name,books.Description,books.Genre,books.img,books.Details from books left join authors on authors.id=books.Author');
    return books;
  } catch (e) {
    logger.error(e);
    return 0;
  }
}

Getbooks();

async function Getbookid(id) {
  const conn = await connection;
  try {
    const books = await conn.query('SELECT * FROM books where id=?', [id]);
    return books;
  } catch (e) {
    logger.error(e);
    return 0;
  }
}

async function Editbooks(query, id) {
  const conn = await connection;
  try {
    const books = await conn.query('UPDATE books set isbn=?,Name=?,Author=?,Description=?,Genre=?,img=?,Details=? where id=?', [query.isbn, query.Name, query.Author, query.Description, query.Genre, query.img, query.Details, id]);
    return books;
  } catch (e) {
    logger.error(e);
    return 0;
  }
}


// Add book

async function Addbook(query) {
  const conn = await connection;
  try {
    const books = await conn.query('INSERT INTO books (isbn,Name,Author,Description,Genre,img,Details) Values(?,?, ?,?,?,?, ?)', [query.isbn, query.Name, (+query.Author), query.Description, query.Genre, query.img, query.Details]);
    return books;
  } catch (e) {
    logger.error(e);
    throw new Error(e);
  }
}


async function Deletebook(id) {
  try {
    const conn = await connection;
    const book = await conn.query('Delete from books where id=?', [id]);
    return book;
  } catch (e) {
    logger.error(e);
    return 0;
  }
}

module.exports = {
  Getbooks,
  Addbook,
  Getbookid,
  Editbooks,
  Deletebook,
};
