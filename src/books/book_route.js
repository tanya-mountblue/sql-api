/* eslint-disable no-unused-vars */
const router = require('express').Router();
const bookController = require('./book_controller');

// books
router.get('/books', async (req, res) => {
  try {
    const books = await bookController.Getbooks();

    res.json(books);
  } catch (e) {
    res.status(404);
  }
});


// book Delete
router.delete('/books/delete/:id', async (req, res) => {
  try {
    const book = await bookController.Deletebook(req.params.id);
    res.send('Success');
  } catch (e) {
    res.status(404);
  }
});

// book by id
router.get('/books/:id', async (req, res) => {
  try {
    const book = await bookController.Getbookid(req.params.id);
    res.json(book[0]);
  } catch (e) {
    res.status(404);
  }
});

// book Edit
router.put('/books/edit/:id', async (req, res) => {
  try {
    const books = await bookController.Editbooks(req.body, req.params.id);
    res.send('Success');
  } catch (e) {
    throw new Error(e);
  }
});

// book Add
router.post('/books/add', async (req, res) => {
  try {
    const books = await bookController.Addbook(req.body);
    res.send('Success');
  } catch (e) {
    throw new Error(e);
  }
});

module.exports = router;
