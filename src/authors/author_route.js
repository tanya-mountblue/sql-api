/* eslint-disable no-unused-vars */
const router = require('express').Router();
const authorController = require('./author_controller');

// authors
router.get('/authors', async (req, res) => {
  try {
    const authors = await authorController.Getauthors();
    res.json(authors);
  } catch (e) {
    res.status(404);
  }
});


// authors Delete
router.delete('/authors/delete/:id', async (req, res) => {
  try {
    const author = await authorController.Deleteauthor(req.params.id);
    res.send('Success');
  } catch (e) {
    res.status(404);
  }
});

// author by id
router.get('/authors/:id', async (req, res) => {
  try {
    const authors = await authorController.Getauthorid(req.params.id);
    res.json(authors[0]);
  } catch (e) {
    res.status(404);
  }
});

// author Edit
router.put('/authors/edit/:id', async (req, res) => {
  const authors = await authorController.Editauthors(req.body, req.params.id);
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  res.send('Success');
});

// book Add
router.post('/authors/add', async (req, res) => {
  const authors = await authorController.Addauthor(req.body);
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  res.send('Success');
});

module.exports = router;
