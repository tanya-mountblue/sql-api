
const logger = require('../../config/logger');
const connection = require('../../database/mysql_connection');

async function Getauthors() {
  try {
    const conn = await connection;
    const authors = await conn.query('SELECT * FROM authors');
    return authors;
  } catch (e) {
    logger.error(e);
    return 0;
  }
}

Getauthors();

async function Getauthorid(id) {
  try {
    const conn = await connection;
    const authors = await conn.query('SELECT * FROM authors where id=?', [id]);
    return authors;
  } catch (e) {
    logger.error(e);
    return 0;
  }
}

async function Editauthors(query, id) {
  try {
    const conn = await connection;
    const authors = await conn.query('UPDATE authors set Name=?,Born=?,Died=?,img=?,Details=?,Description=? where id=?', [query.Name, query.Born, query.Died, query.img, query.Details, query.Description, id]);
    return authors;
  } catch (e) {
    logger.error(e);
    return 0;
  }
}


// Add author

async function Addauthor(query) {
  const conn = await connection;
  try {
    const authors = await conn.query('INSERT INTO authors (Name,Born,Died,img,Details,Description) Values(?,?,?,?,?,?)', [query.Name, query.Born, query.Died, query.img, query.Details, query.Description]);
    return authors;
  } catch (e) {
    logger.error(e);
    return 0;
  }
}


async function Deleteauthor(id) {
  try {
    const conn = await connection;
    const author = await conn.query('Delete from authors where id=?', [id]);
    return author;
  } catch (e) {
    logger.error(e);
    return 0;
  }
}

module.exports = {
  Getauthors,
  Addauthor,
  Getauthorid,
  Editauthors,
  Deleteauthor,
};
