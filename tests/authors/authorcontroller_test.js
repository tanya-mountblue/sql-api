/* eslint-disable no-undef */
const chai = require('chai');
const database = require('../../src/authors/author_controller');

xdescribe('test sql data', () => {
  it('Should return authors Data', async () => {
    const result = {
      id: 5,
      Name: 'vishnu khandekar',
      Born: '4',
      Description: 'hfhhfh',
      Details: 'fg',
      Died: '6',
      img: 'tyt',
    };


    const data = await database.Getauthors();
    chai.expect(data[0]).to.deep.equal(result);
  });
});


xdescribe('test sql data', () => {
  it('Should return Edit author Data', async () => {
    const result = {
      id: 5,
      Name: 'vishnu khandekar',
      Born: '4',
      Description: 'hfhhfh',
      Details: 'fg1',
      Died: '6',
      img: 'tyt',
    };

    const data = await database.Editauthors(result, result.id);
    chai.expect(data.affectedRows).to.equal(1);
  });
});

xdescribe('test sql data', () => {
  it('Should return delete author Data', async () => {
    const result = {
      id: 5,
    };
    const data = await database.Deleteauthor(result.id);
    chai.expect(data.affectedRows).to.equal(1);
  });
});

xdescribe('test sql data', () => {
  it('Should return add author Data', async () => {
    const result = {
      Name: 'Amrita Pritam',
      Born: '14',
      Description: 'opuyt',
      Details: 'fg',
      Died: '16',
      img: 'tyt',
    };
    const data = await database.Addauthor(result);
    chai.expect(data.affectedRows).to.equal(1);
  });
});
