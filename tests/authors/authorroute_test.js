/* eslint-disable no-undef */
const chai = require('chai');
const http = require('chai-http');
const app = require('../../index');


const data = {
  id: 1,
  Name: 'sumitra nandan pant ',
  Born: '4',
  Description: 'hfhhfh',
  Details: 'fg',
  Died: '6',
  img: 'tyt',
};


describe('test the author route', () => {
  it('GET authors', (done) => {
    const agent = chai.use(http).request.agent(app);
    agent
      .get('/authors')
      .end((err, response) => {
        if (err) return done(err);
        chai.expect(response.body[0].id).to.deep.equal(5);
        done();
        agent.close();
        return 0;
      });
  });
});


describe('test the add author ', () => {
  it('POST add authors', (done) => {
    const agent = chai.use(http).request.agent(app);
    agent
      .post('/authors/add/')
      .send(data)
      .end((err, response) => {
        if (err) return done(err);
        chai.expect(response.text).to.include('Success');
        done();
        agent.close();
        return 0;
      });
  });
});

// Edit route

describe('test the edit author ', () => {
  it('GET Edit authors', (done) => {
    const agent = chai.use(http).request.agent(app);
    agent
      .put('/authors/edit/10')
      .send(data)
      .end((err, response) => {
        if (err) return done(err);
        chai.expect(response.text).to.include('Success');
        done();
        agent.close();
        return 0;
      });
  });
});


// delete
describe('test the delete author ', () => {
  it('GET delete author', (done) => {
    const agent = chai.use(http).request.agent(app);
    agent
      .delete('/authors/delete/10')
      .end((err, response) => {
        if (err) return done(err);
        chai.expect(response.text).to.include('Success');
        done();
        agent.close();
        return 0;
      });
  });
});
