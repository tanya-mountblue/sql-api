/* eslint-disable no-undef */
const chai = require('chai');
const http = require('chai-http');
// const database = require('../../src/books/book_controller');
const app = require('../../index');

const data = {
  id: 11,
  isbn: '1234',
  Name: 'Nirmala',
  Author: 5,
  Description: 'gdfghg',
  Details: 'flfklfkd',
  Genre: 'novel',
  img: 'l',
};


describe('test the book route', () => {
  it('GET books', (done) => {
    const agent = chai.use(http).request.agent(app);
    agent
      .get('/books')
      .end((err, response) => {
        if (err) return done(err);
        chai.expect(response.body[1].Name).to.equal('yayati');
        done();
        agent.close();
        return 0;
      });
  });
});


// ADD route
describe('test the add book ', () => {
  it('GET add books', (done) => {
    const agent = chai.use(http).request.agent(app);
    agent
      .post('/books/add')
      .send(data)
      .end((err, response) => {
        if (err) return done(err);
        chai.expect(response.text).to.include('Success');
        done();
        agent.close();
        return 0;
      });
  });
});


// Edit route
describe('test the edit book ', () => {
  it('GET Edit books', (done) => {
    const agent = chai.use(http).request.agent(app);
    agent
      .put('/books/edit/11')
      .send(data)
      .end((err, response) => {
        if (err) return done(err);
        chai.expect(response.text).to.include('Success');
        done();
        agent.close();
        return 0;
      });
  });
});

// delete
describe('test the delete book ', () => {
  it('GET delete book', (done) => {
    const agent = chai.use(http).request.agent(app);
    agent
      .delete('/books/delete/12')
      .end((err, response) => {
        if (err) return done(err);
        chai.expect(response.header).to.include('*');
        done();
        agent.close();
        return 0;
      });
  });
});
