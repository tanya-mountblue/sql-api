/* eslint-disable no-undef */
const chai = require('chai');
const database = require('../../src/books/book_controller');

xdescribe('test sql data', () => {
  it('Should return Books Data', async () => {
    const result = {
      id: 11,
      Name: 'yayati',
      Author: 5,
      Description: 'gdfghg',
      Details: 'ff',
      Genre: 'novel',
      img: 'gfg',
    };


    const data = await database.Getbooks();
    chai.expect(data[0].id).to.deep.equal(result.id);
  });
});

describe('test sql data', () => {
  it('Should return Edit Book Data', async () => {
    const result = {
      id: 11,
      isbn: '12639',
      Name: 'yayati',
      Author: 5,
      Description: 'gdfghg',
      Details: 'f',
      Genre: 'novel',
      img: 'gfg',
    };

    const data = await database.Editbooks(result, result.id);
    chai.expect(data.affectedRows).to.equal(1);
  });
});

xdescribe('test sql data', () => {
  it('Should return delete Book Data', async () => {
    const result = {
      id: 10,
    };
    const data = await database.Deletebook(result.id);
    chai.expect(data.affectedRows).to.equal(1);
  });
});

xdescribe('test sql data', () => {
  it('Should return add Book Data', async () => {
    const result = {
      isbn: '12578',
      Name: 'gora',
      Author: 5,
      Description: 'gdfg',
      Details: 'fhghj',
      Genre: 'novel',
      img: 'gfjg',
    };
    const data = await database.Addbook(result);
    chai.expect(data.affectedRows).to.equal(1);
  });
});
