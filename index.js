const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const logger = require('./config/logger');
const books = require('./src/books/book_route');
const authors = require('./src/authors/author_route');

const app = express();

app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
  next();
});
app.use(books);
app.use(authors);
// hg

app.listen(3002, () => {
  logger.info('app is listening at 3002');
});

module.exports = app;
