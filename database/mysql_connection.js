const mysql = require('promise-mysql');

const db = process.env.db || 'ExpressApp';

const main = mysql.createConnection({
  host: 'localhost',
  user: 'app',
  database: db,
});

module.exports = main;
